<?php // template name: Precios 
get_header();
?>

<?php  $x = query_posts(array('post_type'=>'page' , 'pagename'=>'precios')); ?>
    <?php while(have_posts()) { the_post();?>
    <section id="portfolio" class="section-card" > 
      
     <div class="container">
   
      <div class="row">
        <div class="col-lg-5 mx-auto text center">
              <?php $a = get_field('titulo_azul'); 
                if($a != null){?>
                   <h5 class="text-sm-left azul"><?php the_field('titulo_azul');?></h5>
               <?php } ?>
               
               <?php if( have_rows('items_titulo') ): ?>
                
            	<?php while( have_rows('items_titulo') ): the_row(); 
            
            		// vars   
            		$item= get_sub_field('item_titulo_repeater');
            		
            	  
            
            		?>
            		  
                   <h3 class="text-sm-left color-text"><?php echo $item; ?></h3>
                
              
            	<?php endwhile; ?>
            
            
            
            <?php endif; ?>
           
          
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 mx-auto text-center">
          <div class="card panel-box">
            <ul class="nav nav-tabs ">
              <li class="nav-item">
                <a class="nav-link active card-nav text-nav" href="#">Gratis</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-nav color-text" href="#">Pro</a>
              </li>
              
            </ul>
          
        <div class="card-block">
          <div class="row">
                  <div class="col-lg-12 linea-card">
                      <h6></h6>
                  </div>
          </div>
          <div class="row">
            <div class="col-lg-12 mx-auto text-sm-left card-bottom">
                <?php $a = get_field('titulo_gratis_azul'); 
                    if($a != null){?>
                      <h6 class="text-sm-left azul"><?php the_field('titulo_gratis_azul');?>
                   <?php } ?>
               
                 <?php if( have_rows('items_gratis') ): ?>
                
            	<?php while( have_rows('items_gratis') ): the_row(); 
            
            		// vars   
            		$item_gratis= get_sub_field('item_gratis');
            		
            	  
            
            		?>
            		  
                <h6 class="text-sm-left color-text"><?php echo $item_gratis; ?></h3>
                
              
            	<?php endwhile; ?>
            
            
            
            <?php endif; ?>
         
            <br>
            <div class="col-lg-10 mx-auto text-sm-right">
              <a href="<?php the_field('enlace_boton'); ?>" class="mybutton-azul glow-button-azul">
                  
                  <?php $a = get_field('texto_boton'); 
                    if($a != null){?>
                     <?php the_field('texto_boton');?>
                   <?php } ?>
                  </a>
            </div>
            
            </div>
          </div>
          
        </div>
      </div>
        </div>
      </div>
     
  </div> 
 
</section>

 <div class="row">
              <div class="col-lg-12 linea-section">
                  <h6></h6>
              </div>
      </div>
 <section id="portfolio" >
   <div class="container">
     <div class="row space">
       <div class="col-lg-12 mx-auto text-center">
            <?php $a = get_field('titulo_pagos'); 
                    if($a != null){?>
                    <h3 class="color-text"> <?php the_field('titulo_pagos');?></h3>
                   <?php } ?>
      
         
          
       </div>
     </div>
     <div class="row">
       <div class="col-lg-8 mx-auto text-justify">
            <?php $a = get_field('texto_pagos'); 
                    if($a != null){?>
                     <h6 class="text-justify color-text"> <?php the_field('texto_pagos');?></h6>
                   <?php } ?>
      
         </div>
     </div>
     <div class="row space">
       <div class="col-lg-12 mx-auto text-center">
            <?php $a = get_field('titulo_politicas'); 
                    if($a != null){?>
                     <h3 class="color-text"> <?php the_field('titulo_politicas');?></h3>
                   <?php } ?>
          
         
          
       </div>
    </div>
       <div class="row">
           <div class="col-lg-2"></div>
            <?php if( have_rows('items_politicas') ): ?>
                <?php $cont=0 ?>
                
            	<?php while( have_rows('items_politicas') ): the_row(); 
            
            		// vars   
            		$politica= get_sub_field('item_politica');
            	
            	  $cont++;
            
            		?>
            		 <?php 
                    if($cont%2==0){?>
                       
                       <div class="col-lg-4 mx-auto text-justify">
                             <h6 class="text-justify color-text"><?php echo $politica; ?></h6>
                          
                         </div>
                       
                   <?php }else{?>
                    <div class="col-lg-4 mx-auto text-justify">
                        <h6 class="text-justify color-text"><?php echo $politica; ?></h6>
          
                     </div>
                      
                    
                   
                  <?php }?>
                
              
            	<?php endwhile; ?>
            
            
            
            <?php endif; ?>
        
         <div class="col-lg-2"></div>
     </div>
     <div class="row space">
       <div class="col-lg-12 mx-auto text-center">
           <a href="<?php the_field('url_apreciacion'); ?>" class="nav-link js-scroll-trigger">
                 
                  <?php $a = get_field('apreciaciones'); 
                    if($a != null){?>
                    <h5 class="text-center azul"> <?php the_field('apreciaciones');?></h5>
                   <?php } ?>
            </a>
         
       </div>
     </div>
   </div>
</section>    
    
	   
    
<?php  }
get_footer();
?>
 
