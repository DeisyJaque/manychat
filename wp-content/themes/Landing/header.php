<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ConverterChat</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo bloginfo('template_url');?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">-->
    
   

    <!-- Custom fonts for this template -->
    <link href="<?php echo bloginfo('template_url');?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo bloginfo('template_url');?>/css/manychat.css" rel="stylesheet">
     

  </head>

  <body id="page-top">
          
    <!-- Navigation -->
    <nav class="navbar navbar navbar-expand-lg navbar-light " id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" id="toogler-click" value="true">
          
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="https://manychat-proyectokamila.c9users.io/precios/" id="precios">Precios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contactos">Contactos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
            </li>
		      	<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="https://manychat-proyectokamila.c9users.io:8081/singin">Iniciar Sesión</a>
            </li>
            <li class="nav-item boton-nav" id="boton">
              <a class="mybutton-azul glow-button-azul" href="https://manychat-proyectokamila.c9users.io:8081/singin">Comenzar gratis</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="row linea" >
              <div class="col-lg-12 linea-nav">
                  <h6></h6>
              </div>
      </div>