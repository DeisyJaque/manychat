
  <?php 
  get_header();
  ?>
    <!-- Section 1-->
    <?php  $x = query_posts(array('post_type'=>'page' , 'pagename'=>'principal')); ?>
    <?php while(have_posts()) { the_post();?>
    <section id="portfolio" class="mysection1" style="background: url(<?php the_field('fondo_principal');?>);background-size:cover;background-repeat:no-repeat;background-position:center;background-attachment:fixed;"> 
        <div class="container">
          <div class="container">
            <?php $a = get_field('titulo_principal'); 
            if($a != null){?>
             <h2 class="text-center"><?php the_field('titulo_principal');?></h2>
           <?php } ?>
            <?php $a = get_field('texto_principal'); 
            if($a != null){?>
            <h5 class="text-center"><?php the_field('texto_principal');?></h5>
           <?php } ?>
            
          	
          </div>  
           <div class="col-lg-8 mx-auto text-center">
                <a href="<?php the_field('boton_principal'); ?>" class="mybutton glow-button">
                   <?php $a = get_field('texto_boton_principal'); 
                    if($a != null){?>
                     <?php the_field('texto_boton_principal');?>
                   <?php } ?>
                </a>
              </div>
              <div class="row">
                  <div class="col-lg-8 mx-auto text-center biglg">
                      <img src="<?php echo bloginfo('template_url');?>/images/iphone.png" class="tlf1"  alt="Responsive image">
                  </div>
               <div class="col-lg-4 mx-auto text-center texto1">
                      <?php $a = get_field('subtitulo_uno'); 
                        if($a != null){?>
                        <h3 class="text-center color-text"><?php the_field('subtitulo_uno');?></h3>
                       <?php } ?>
                      
                       <?php $a = get_field('texto_uno'); 
                        if($a != null){?>
                         <h6 class="text-sm-left color-text"><?php the_field('texto_uno');?></h6>
                       <?php } ?>
                      
                       <div class="col-lg-8 mx-auto button-center">
                        	<a href="<?php the_field('url_boton_azul'); ?>" class="mybutton-azul glow-button-azul"> 
                        	<?php $a = get_field('texto_boton_azul'); 
                            if($a != null){?>
                             <?php the_field('texto_boton_azul');?>
                           <?php } ?>
                        	</a><br>
                        	 <img src="<?php echo bloginfo('template_url');?>/images/letrapeque.png" class="msj" alt="Responsive image" >
                      </div>
                        
                        
              </div>
               <div class="col-lg-8 mx-auto oculto" >
                      <img src="<?php echo bloginfo('template_url');?>/images/iphone.png" class="img-responsive tlf1"  alt="Responsive image">
              </div>
              
             
          </div>
     
    </div>
      <div class="row">
              <div class="col-lg-12 linea-section">
                  <h6></h6>
              </div>
      </div>
    </section>

    <section id="portfolio" class="mysection">
      <div class="container">
        <div class="row">
          
           <div class="col-lg-4 mx-auto texto2">
                <?php $a = get_field('titulo_dos'); 
                if($a != null){?>
                  <p class="text-sm-left fuente2 color-text"><?php the_field('titulo_dos');?></p>
               <?php } ?>
              <?php $a = get_field('subtitulo_dos'); 
                if($a != null){?>
                  <h3 class="text-sm-left color-text"><?php the_field('subtitulo_dos');?></h3>
               <?php } ?>
               <?php $a = get_field('texto_dos'); 
                if($a != null){?>
                  <h6 class="text-sm-left color-text"><?php the_field('texto_dos');?></h6>
               <?php } ?>
                
           </div>
           <div class="col-lg-5 mx-auto text-sm-left">
             <img src="<?php echo bloginfo('template_url');?>/images/2_iphones.png" class="img-responsive-alt" alt="Responsive image">
             
           </div>
        </div>

      </div>
    </section>
     <!--Sección 3-->
	    <section id="portfolio" style="background: url(<?php the_field('fondo_dos');?>);background-repeat: no-repeat;background-position: center;background-size: cover;background-attachment: fixed;color:#f9f4f4;">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 mx-auto texto2 biglg">
              <img src="<?php echo bloginfo('template_url');?>/images/iphone2.png" class="img-responsive" alt="Responsive image">
            </div>
            <div class="col-lg-4 mx-auto texto3">
              <?php $a = get_field('titulo_tres'); 
                if($a != null){?>
                  <p class="text-sm-left fuente"><?php the_field('titulo_tres');?></p>
               <?php } ?>
                <?php $a = get_field('subtitulo_tres'); 
                if($a != null){?>
                  <h3 class="text-sm-left "><?php the_field('subtitulo_tres');?></h3>
               <?php } ?>
                <?php $a = get_field('texto_tres'); 
                if($a != null){?>
                  <h6 class="text-sm-left "><?php the_field('texto_tres');?></h6>
               <?php } ?>
                     
             
            </div>
             <div class="col-lg-5 mx-auto tlf2 oculto">
              <img src="<?php echo bloginfo('template_url');?>/images/iphone2.png" class="tlf3 img-responsive" alt="Responsive image">
            </div>
          </div>
        </div>
    </section>
    <!--Sección 4-->
	    <section id="portfolio">
        <div class="container">
          <div class="row">
              <div class="col-lg-5 mx-auto">
                <img src="<?php echo bloginfo('template_url');?>/images/logo_mns2.png"  class="img-responsive" alt="Responsive image">
              </div>
              <div class="col-lg-4 mx-auto texto3">
                    <?php $a = get_field('subtitulo_cuatro'); 
                    if($a != null){?>
                      <h3 class="text-sm-left color-text"><?php the_field('subtitulo_cuatro');?></h3>
                   <?php } ?>
                   <?php $a = get_field('texto_cuatro'); 
                    if($a != null){?>
                      <h6 class="text-sm-left color-text"><?php the_field('texto_cuatro');?></h6>
                   <?php } ?>
               
              </div>
            </div>
        </div>
         <div class="row">
                <div class="col-lg-12 linea-section2">
                    <h6></h6>
                </div>
        </div>
      
    </section>
    <!--Sección 5 -- slide -->
	    <section id="portfolio" class="mysection5" >
        <div class="container">
           <div class="row">

                    <!-- Section Header -->
                    <div class="col-lg-12 mx-auto text-center">
                    <?php $a = get_field('subtitulo_seis'); 
                    if($a != null){?>
                      <h3 class="color-text text-center"><?php the_field('subtitulo_seis');?></h3>
                   <?php } ?>
                        
                       
                      
                    </div>

                      <!-- teams Slider -->
                    <?php if( have_rows('slider') ): ?>
                     <div id="carouselExampleControls" class="carousel slide big" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                         <div class="row">
                           <?php $count =0;?>
                           <?php while( have_rows('slider') ): the_row(); 

                      		// vars
                      		$nombre = get_sub_field('nombre_y_apellido');
                      		$cargo = get_sub_field('cargo');
                      		$imagen = get_sub_field('imagen');
                      		$content = get_sub_field('descripcion');
                            
                      		?>
                      		<?php  $count++;?>
                            <?php if ($count == 1){?>
                                      <div class="col-lg-4 mx-auto elemcont">
                                        <?php if( $nombre ): ?>
                                  			<h5 class="text-sm-left color-text"><?php echo $nombre; ?></h5>
                                  			<?php endif; ?>
                                  			<?php if( $cargo ): ?>
                                  			<h5 class="text-sm-left azul"><?php echo $cargo; ?></h5>
                                  			<?php endif; ?>
                                        
                                         
                                           <img class="circle img-responsive text-center" src="<?php echo $imagen; ?>"  />
                                         
                                         <?php if( $content ): ?>
                                  			<h6 class="text-sm-left elemcont color-text"><?php echo $content; ?></h6>
                                  			<?php endif; ?>
                                        
                                        </div> 
                               
                                    </div> 
                                 </div> 
                                 <?php }?>
                                  <div class="carousel-item">
                                      <div class="row">
                                           <div class="col-lg-4 mx-auto elemcont">
                                            <?php if( $nombre ): ?>
                                  			<h5 class="text-sm-left color-text"><?php echo $nombre; ?></h5>
                                  			<?php endif; ?>
                                  			<?php if( $cargo ): ?>
                                  			<h5 class="text-sm-left azul"><?php echo $cargo; ?></h5>
                                  			<?php endif; ?>
                                        
                                         
                                           <img class="circle img-responsive text-center" src="<?php echo $imagen; ?>"  />
                                             
                                             <?php if( $content ): ?>
                                  			<h6 class="text-sm-left elemcont color-text"><?php echo $content; ?></h6>
                                  			<?php endif; ?>
                                        
                                            </div> 
                                       
                                      </div> 
                                  </div> 
                               
                                
                               
                      <?php endwhile; ?>
                    
                    
                    
                        
                          
                       
                        
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" >
                        <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
                        <span class="sr-only" >Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" >
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                        <!-- Fin Slides -->

                  <?php endif; ?>  
                </div>
          </div>
        
    </section>
    
    <!-- Sección 6-->
	    <section id="portfolio" class="contacto">
      <div class="container">
           <div class="row">
            
            <div class="col-lg-12 mx-auto text-center">
              <?php $a = get_field('text_cinco'); 
                    if($a != null){?>
                      <h3 class="text-center texto3"><?php the_field('text_cinco');?></h3>
                   <?php } ?>
              
              <a href="<?php the_field('url_contacto'); ?>" class="btn btn-lg btn-outline">
                
                <?php $a = get_field('texto_boton_vacio'); 
                    if($a != null){?>
                      <?php the_field('texto_boton_vacio');?>
                 <?php } ?>
                
              </a>
            </div>
            </div>
      </div>
    </section>
    
    <!--Seción 7-->
	    <section id="portfolio" class="mysection7">
        <div class="container">
          <div class="row">
             <div class="col-lg-12 mx-auto text-center texto-espacio">
               <?php $a = get_field('subtitulo_cinco'); 
                    if($a != null){?>
                       <h3 class="text-center color-text"><?php the_field('subtitulo_cinco');?></h3>
                 <?php } ?>
             </div>
             <?php if( have_rows('preguntas_repeater') ): ?>
                <?php $cont=0 ?>
            	<?php while( have_rows('preguntas_repeater') ): the_row(); 
            
            		// vars   
            		$pregunta= get_sub_field('pregunta_conver');
            		$respuesta= get_sub_field('respuesta_conver');
            	  $cont++;
            
            		?>
            		 <?php 
                    if($cont%2==0){?>
                      
                       <div class="col-lg-5 mx-auto text-sm-left">
                          <h5 class="text-center color-text">  <?php echo $pregunta; ?></h5>
                          <h6 class="text-sm-left color-text"> <?php echo $respuesta; ?> </h6>
                        
                      </div>
                   <?php }else{?>
                    <div class="col-lg-5 mx-auto text-sm-left">
                          <h5 class="text-center color-text"><?php echo $pregunta; ?></h5>
                          <h6 class="text-sm-left color-text"> <?php echo $respuesta; ?> </h6>
                        
                      </div>
                   
                  <?php }?>
                
              
            	<?php endwhile; ?>
            
            
            
            <?php endif; ?>
           
          </div>
        </div>
      </section>
    
	   
    
<?php  }
get_footer();
?>
 